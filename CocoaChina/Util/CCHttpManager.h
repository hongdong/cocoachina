//
//  YJHttpManager.h
//  yuedu
//
//  Created by Zhang on 15/9/16.
//  Copyright © 2015年 北京易利友信息技术有限公司. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CCHttpManager : AFHTTPRequestOperationManager

+ (instancetype)sharedManager;

- (AFHTTPRequestOperation *)yj_get:(NSString *)urlPath parameters:(NSDictionary *)parameters compliteBlock:(void (^)(AFHTTPRequestOperation *,id , NSError *))compliteBlock;

- (AFHTTPRequestOperation *)yj_get:(NSString *)urlPath parameters:(NSDictionary *)parameters loadCache:(BOOL)loadCache saveCache:(BOOL)saveCache  compliteBlock:(void (^)(AFHTTPRequestOperation *operation,id responseObject, NSError *error))compliteBlock;

- (AFHTTPRequestOperation *)yj_post:(NSString *)urlPath parameters:(NSDictionary *)parameters compliteBlock:(void (^)(AFHTTPRequestOperation *, id, NSError *))compliteBlock;

- (AFHTTPRequestOperation *)yj_post:(NSString *)urlPath parameters:(NSDictionary *)parameters loadCache:(BOOL)loadCache saveCache:(BOOL)saveCache  compliteBlock:(void (^)(AFHTTPRequestOperation *,id , NSError *))compliteBlock;

@end
