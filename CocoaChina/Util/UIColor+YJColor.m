//
//  UIColor+DAMColor.m
//  yuedu
//
//  Created by Zhang on 15/9/18.
//  Copyright © 2015年 北京易利友信息技术有限公司. All rights reserved.
//

#import "UIColor+YJColor.h"


@implementation UIColor (YJColor)

+ (UIColor *)yj_grayColor{

    return rgb(240, 241, 243);
}

+ (UIColor *)yj_lineColor{
    return HexColor(0xD5D5D5);
}

+ (UIColor *)yj_darkTextColor{
    return HexColor(0x2F2F2F);
}

+ (UIColor *)yj_lightTextColor{
    return HexColor(0x888888);
}

+ (UIColor *)yj_cellBackgroundColor{
    return [UIColor whiteColor];
}

+(UIColor *)yj_cellSelectedColor{
    return HexColor(0xE4E4E4);
}

+(UIColor *)yj_blueColor{
    return HexColor(0x3498db);
}

+(UIColor *)yj_blueColorDark{
    return HexColor(0x2980b9);
}

+(UIColor *)yj_redColor{
    return HexColor(0xE84D3C);
}

@end
