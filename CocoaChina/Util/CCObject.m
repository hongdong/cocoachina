//
//  YJObject.m
//  yuedu
//
//  Created by Zhang on 15/9/17.
//  Copyright © 2015年 北京易利友信息技术有限公司. All rights reserved.
//

#import "CCObject.h"

@implementation CCObject

-(void)setValue:(id)value forUndefinedKey:(NSString *)key{
    
}

- (instancetype)initWithDictionary:(NSDictionary *)dictionary{
    if (self= [super init]) {
        _initalDictionary = dictionary;
        [self setValuesForKeysWithDictionary:dictionary];
    }
    return self;
}

@end
