//
//  NSDictionary+YJJsonString.h
//  yuedu
//
//  Created by Zhang on 15/10/25.
//  Copyright © 2015年 北京易利友信息技术有限公司. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDictionary (YJJsonString)

-(NSString*)toJSONString;

+(instancetype )dictionaryFromJSONString:(NSString *)jsonString;

@end
