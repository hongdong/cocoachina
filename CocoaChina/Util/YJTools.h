//
//  YJTools.h
//  yuedu
//
//  Created by Zhang on 15/9/16.
//  Copyright © 2015年 北京易利友信息技术有限公司. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface YJTools : NSObject

+ (NSString *) md5:(NSString *)str;

+ (NSString *) appVersion;

+(NSString *)buildVersion;

+ (NSString *)currentTimeStamp;

@end
