//
//  UIColor+DAMColor.h
//  yuedu
//
//  Created by Zhang on 15/9/18.
//  Copyright © 2015年 北京易利友信息技术有限公司. All rights reserved.
//

#import <UIKit/UIKit.h>

#define rgba(r,g,b,a) [UIColor colorWithRed:r/255.0f green:g/255.0f blue:b/255.0f alpha:a]
#define rgb(r,g,b) [UIColor colorWithRed:r/255.0f green:g/255.0f blue:b/255.0f alpha:1.0]
#define HexColor(rgbValue)                                                                                                                                                                       \
[UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16)) / 255.0f green:((float)((rgbValue & 0xFF00) >> 8)) / 255.0f blue:((float)(rgbValue & 0xFF)) / 255.0f alpha:1.0]

@interface UIColor (YJColor)

+ (UIColor *)yj_grayColor;

+ (UIColor *)yj_lineColor;

+ (UIColor *)yj_darkTextColor;

+ (UIColor *)yj_lightTextColor;

+ (UIColor *)yj_cellBackgroundColor;

+ (UIColor *)yj_cellSelectedColor;

+(UIColor *)yj_blueColor;

+(UIColor *)yj_blueColorDark;

+(UIColor *)yj_redColor;
@end
