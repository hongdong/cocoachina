//
//  YJObject.h
//  yuedu
//
//  Created by Zhang on 15/9/17.
//  Copyright © 2015年 北京易利友信息技术有限公司. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CCObject : NSObject
///元数据
@property (nonatomic, copy, readonly) NSDictionary *initalDictionary;

///通过字典初始化
- (instancetype)initWithDictionary:(NSDictionary *)dictionary;

@end
