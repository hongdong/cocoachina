//
//  YJDataManager.h
//  yuedu
//
//  Created by Zhang on 15/9/20.
//  Copyright © 2015年 北京易利友信息技术有限公司. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <sqlite3.h>
#import "YJSingleton.h"
#import "FMDB.h"

@interface CCDBManager : NSObject

singleton_interface(CCDBManager);

#pragma mark - 属性
#pragma mark 数据库引用，使用它进行数据库操作
@property (nonatomic,strong) FMDatabaseQueue *database;

#pragma mark - 共有方法
/**
 *  打开数据库
 *
 *  @param dbname 数据库名称
 */
//-(void)openDb:(NSString *)dbname;

/**
 *  执行无返回值的sql
 *
 *  @param sql sql语句
 */
-(void)executeNonQuery:(NSString *)sql parameterDictionary:(NSDictionary *)parameters;

/**
 *  执行有返回值的sql
 *
 *  @param sql sql语句
 *
 *  @return 查询结果
 */
-(NSArray *)executeQuery:(NSString *)sql parameterDictionary:(NSDictionary *)parameters;

@end

