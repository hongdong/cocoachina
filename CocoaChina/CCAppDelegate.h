//
//  AppDelegate.h
//  yuedu
//
//  Created by Zhang on 15/9/16.
//  Copyright © 2015年 北京易利友信息技术有限公司. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CCRootViewController.h"
#import "CCNavigationController.h"

@class JVFloatingDrawerViewController;
@class YJLeftDrawerViewController;
@class CCLoginViewController;
@interface CCAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (nonatomic, strong) CCNavigationController *rootViewController;
@property (nonatomic, strong) CCRootViewController *centerViewController;

+ (CCAppDelegate *)globalDelegate;

@end

