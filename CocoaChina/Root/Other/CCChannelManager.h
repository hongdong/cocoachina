//
//  YJChannelManager.h
//  yuedu
//
//  Created by Zhang on 15/9/20.
//  Copyright © 2015年 北京易利友信息技术有限公司. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CCChannelManager : NSObject

+(instancetype)sharedManager;

///所有频道
@property (nonatomic,copy) NSMutableArray *confirmedChannels;

///重新加载数据
-(void)reloadData;

///保存标签
-(void)save;

@end