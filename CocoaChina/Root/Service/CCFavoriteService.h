//
//  CCFavoriteService.h
//  CocoaChina
//
//  Created by Zhang on 15/10/31.
//  Copyright © 2015年 北京大米信息技术有限公司. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CCHistoryModel.h"
#import "YJSingleton.h"
#import "CCMasterModel.h"

@interface CCFavoriteService : NSObject
singleton_interface(CCFavoriteService)

///添加于都历史记录
-(void)addFavorite:(CCMasterModel *)weChatModel;
///删除阅读历史记录
-(void)removeFavorite:(CCMasterModel *)weChatModel;
///是否存在历史记录
-(BOOL)isInFavorite:(CCMasterModel *)newsModel;
///通过newsId获取历史记录
-(CCHistoryModel *)getFavoriteByNewsId:(NSString *)newsId;
///获取历史记录列表
-(NSArray *)favoriteListByOffset:(NSInteger)offset limit:(NSInteger)limit;

@end
