//
//  YJReadHistoryModel.m
//  yuedu
//
//  Created by Zhang on 15/10/21.
//  Copyright © 2015年 北京易利友信息技术有限公司. All rights reserved.
//

#import "CCHistoryModel.h"
#import "NSDictionary+YJJsonString.h"

@implementation CCHistoryModel

-(void)setValue:(id)value forKey:(NSString *)key{
    if ([key isEqualToString:@"data"]) {
       NSDictionary *weChatDictionary = [NSDictionary dictionaryFromJSONString:value];
      self.masterCellModel = [[CCMasterModel alloc]initWithDictionary:weChatDictionary];
    }else{
        [super setValue:value forKey:key];
    }
}

@end