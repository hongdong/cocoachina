//
//  YJNavigationBar.h
//  yuedu
//
//  Created by Zhang on 15/9/18.
//  Copyright © 2015年 北京易利友信息技术有限公司. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CCNavigationBar : UIView
@property (nonatomic, strong) UIImage *titleImage;
@property (nonatomic, copy) NSString *title;
@property (nonatomic, strong) UIButton *leftButton;
@property (nonatomic, strong) NSArray *leftButtons;
@property (nonatomic, strong) UIButton *rightButton;
@property (nonatomic, strong) NSArray *rightButtons;

- (void)damNavigationBarDidScrollWithScrollView:(UIScrollView *)scrollView;

@end
