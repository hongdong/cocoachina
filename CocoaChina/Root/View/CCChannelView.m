//
//  YJChannelView.m
//  yuedu
//
//  Created by Zhang on 15/9/19.
//  Copyright © 2015年 北京易利友信息技术有限公司. All rights reserved.
//

#import "CCChannelView.h"

#define MENU_BUTTON_WIDTH  60

@interface CCChannelView ()<UIScrollViewDelegate>
@property (nonatomic, strong) UIView *contentView;
@property (nonatomic, strong) UIButton *dropDownButton;
@property (nonatomic, strong) UIScrollView *scrollView;
@property (nonatomic, strong) UIView *sepLine;
@property (nonatomic, assign) CGFloat lastOffsetX;
@property (nonatomic, assign) NSInteger currentSelected;
@property (nonatomic, assign) NSInteger preSelected;
@property (nonatomic, strong) NSMutableArray *channelButtons;
@end

@implementation CCChannelView

- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        [self commonInit];
    }
    return self;
}

- (void)commonInit{
    [self addSubview:self.contentView];
    [self.contentView addSubview:self.scrollView];
    [self.contentView addSubview:self.sepLine];
    
    [self.contentView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self);
    }];
    
    [self.scrollView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.contentView);
    }];
    
    [self.sepLine mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.bottom.equalTo(self.contentView);
        make.height.equalTo(@0.5);
    }];
}

#pragma mark - UIScrollViewDelegate
-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
    
}

#pragma mark - method

- (void)changeView:(float)offsetX{
    
    float xx = offsetX * (MENU_BUTTON_WIDTH /SCREEN_WIDTH);
    float startX = xx;
    int sT = (offsetX)/SCREEN_WIDTH + 1;
    
    if (sT-1 < 0 || sT+1 > self.channelButtons.count){
        return;
    }
    UIButton *btn = self.channelButtons[sT-1];
    float percent = (startX - MENU_BUTTON_WIDTH * (sT - 1))/MENU_BUTTON_WIDTH;
    [self changeColorForButton:btn red:(1 - percent)];
    
    if((int)xx % MENU_BUTTON_WIDTH == 0){
        self.preSelected = self.currentSelected;
        self.currentSelected = sT-1;
        return;
    }
    UIButton *btn2 = self.channelButtons[sT];
    [self changeColorForButton:btn2 red:percent];
}

- (void)changeColorForButton:(UIButton *)btn red:(float)nRedPercent{
    float red = [self lerp:nRedPercent min:86 max:102];
    float green = [self lerp:nRedPercent min:86 max:154];
    float blue = [self lerp:nRedPercent min:86 max:204];
    [btn setTitleColor:rgba(red,green,blue,1) forState:UIControlStateNormal];
}

-(void)channelViewDidScroll:(UIScrollView *)scrollView{
    [self changeView:scrollView.contentOffset.x];
}

-(void)channelViewDidEndDecelerating:(UIScrollView *)scrollView{
    float xx = scrollView.contentOffset.x * (MENU_BUTTON_WIDTH / SCREEN_WIDTH) - MENU_BUTTON_WIDTH;
    [self.scrollView scrollRectToVisible:CGRectMake(xx, 0, self.scrollView.frame.size.width, self.scrollView.frame.size.height) animated:YES];
}

-(void)reloadData{
    
    NSInteger itemCount = [self.dataSource numberOfChannelsForChannelView:self];
    UIView *container = [UIView new];
    [self.scrollView addSubview:container];
    container.backgroundColor = [UIColor clearColor];
    [container mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.scrollView);
        make.height.equalTo(self.scrollView);
    }];
    [self.channelButtons removeAllObjects];
    UIView *lastView = nil;
    for (int i = 0;  i < itemCount; i++) {
        NSString *channelTitle = [self.dataSource titleForChannelForChannelView:self atIndex:i];
        UIButton *channelItem = [UIButton buttonWithType:UIButtonTypeCustom];
        [channelItem addTarget:self action:@selector(channelButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
        channelItem.tag = i;
        [channelItem setTitle:channelTitle forState:UIControlStateNormal];
        [channelItem setTitleColor:rgb(86, 86, 86) forState:UIControlStateNormal];
        channelItem.titleLabel.font = [UIFont systemFontOfSize:15];
        [container addSubview:channelItem];
        CGFloat intrinsicWidth = channelItem.intrinsicContentSize.width;
        [channelItem mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.bottom.equalTo(container);
            make.left.equalTo(lastView?lastView.mas_right:container.mas_left);
            make.width.equalTo(@(intrinsicWidth+20));
        }];
        lastView = channelItem;
        [self.channelButtons addObject:channelItem];
    }
    [container mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(lastView.mas_right);
    }];
    [self changeView:0];
}

#pragma mark - action

-(void)channelButtonClicked:(UIButton *)sender{
    
    if ([self.delegate respondsToSelector:@selector(channelView:didSelectedItemAtIndex:)]) {
        [self.delegate channelView:self didSelectedItemAtIndex:sender.tag];
    }
    UIButton *btn2 = self.channelButtons[self.preSelected];
    btn2.transform = CGAffineTransformIdentity;
    [self changeColorForButton:btn2 red:0];
    
}

#pragma mark - getters and setters

-(UIView *)contentView{
    if (!_contentView) {
        _contentView = [[UIView alloc]init];
        _contentView.backgroundColor = [UIColor yj_cellBackgroundColor];
    }
    return _contentView;
}

- (UIScrollView *)scrollView{
    if (!_scrollView) {
        _scrollView = [[UIScrollView alloc]init];
        _scrollView.showsHorizontalScrollIndicator = NO;
        _scrollView.showsVerticalScrollIndicator = NO;
        _scrollView.alwaysBounceHorizontal = YES;
        _scrollView.delegate = self;
        _scrollView.scrollsToTop = NO;
        _scrollView.backgroundColor = [UIColor yj_cellBackgroundColor];
    }
    return _scrollView;
}

-(UIButton *)dropDownButton{
    if (!_dropDownButton) {
        _dropDownButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _dropDownButton.backgroundColor = [UIColor yj_cellBackgroundColor];
        [_dropDownButton setImage:[UIImage imageNamed:@"channel_nav_arrow"] forState:UIControlStateNormal];
    }
    return _dropDownButton;
}

-(UIView *)sepLine{
    if (!_sepLine) {
        _sepLine = [[UIView alloc]init];
        _sepLine.backgroundColor = [UIColor yj_lineColor];
    }
    return _sepLine;
}

-(NSMutableArray *)channelButtons{
    if (!_channelButtons) {
        _channelButtons = [[NSMutableArray alloc]init];
    }
    return _channelButtons;
}

- (float)lerp:(float)percent min:(float)nMin max:(float)nMax{
    float result = nMin;
    
    result = nMin + percent * (nMax - nMin);
    
    return result;
}

@end
