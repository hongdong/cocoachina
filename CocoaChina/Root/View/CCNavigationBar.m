//
//  YJNavigationBar.m
//  yuedu
//
//  Created by Zhang on 15/9/18.
//  Copyright © 2015年 北京易利友信息技术有限公司. All rights reserved.
//

#import "CCNavigationBar.h"
@interface CCNavigationBar ()

@property (nonatomic, strong) UIImageView *titleImageView;

@property (nonatomic, strong) UILabel *titleLabel;

@property (nonatomic, strong) MASConstraint *titleLabelCenterYConstraint;

@property (nonatomic, strong) UIView *bottomLine;
@end

@implementation CCNavigationBar

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        [self commonInit];
    }
    return self;
}

-(void)commonInit{
    self.clipsToBounds = NO;
    self.backgroundColor = [UIColor yj_cellBackgroundColor];
    [self addSubview:self.titleLabel];
    [self addSubview:self.titleImageView];
    [self addSubview:self.bottomLine];
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        self.titleLabelCenterYConstraint = make.centerY.equalTo(self.mas_centerY).offset(10);
        make.centerX.equalTo(self.mas_centerX);
    }];
    
    [self.titleImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.mas_centerX);
        make.centerY.equalTo(self.mas_centerY).offset(10);
//        make.height.lessThanOrEqualTo(@20);
    }];
    
    [self.bottomLine mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.bottom.equalTo(self);
        make.height.equalTo(@0.5);
    }];
    
    self.titleLabel.hidden = YES;
    self.titleImageView.hidden = YES;
}

-(void)setTitle:(NSString *)title{
    _title = title;
    self.titleLabel.text = title;
    self.titleLabel.hidden = NO;
    
}

-(void)setTitleImage:(UIImage *)titleImage{
    _titleImage = titleImage;
    self.titleImageView.image = titleImage;
    self.titleImageView.hidden = NO;
}

-(void)setLeftButton:(UIButton *)leftButton{
    [self removeLeftButtons];
    _leftButton = leftButton;
    [self addSubview:self.leftButton];
    [self.leftButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.mas_centerY).offset(10);
        make.left.equalTo(self.mas_left);//.offset(10);
    }];
}

-(void)setLeftButtons:(NSArray *)leftButtons{
    [self removeLeftButtons];
    _leftButtons = leftButtons;
    UIView *lastView = nil;
    for (UIButton *button in leftButtons) {
        [self addSubview:button];
        [button mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(self.mas_centerY).offset(10);
            make.left.equalTo(lastView?lastView.mas_right:self.mas_left);//.offset(10);
        }];
        lastView = button;
    }
}

-(void)removeLeftButtons{
    for (UIButton *button in self.leftButtons) {
        [button removeFromSuperview];
    }
    [self.leftButton removeFromSuperview];
}

-(void)setRightButton:(UIButton *)rightButton{
    [self removeRightButtons];
    _rightButton = rightButton;
    if (rightButton) {
        [self addSubview:self.rightButton];
        [self.rightButton mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(self.mas_centerY).offset(10);
            make.right.equalTo(self.mas_right);
        }];
    }
}

-(void)setRightButtons:(NSArray *)rightButtons{
    [self  removeRightButtons];
    _rightButtons = rightButtons;
    UIView *lastView = nil;
    for (UIButton *button in rightButtons) {
        [self addSubview:button];
        [button mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(self.mas_centerY).offset(10);
            make.right.equalTo(lastView?self.mas_right:lastView.mas_left);
        }];
        lastView = button;
    }
}

-(void)removeRightButtons{
    
    for (UIButton *button in self.rightButtons) {
        [button removeFromSuperview];
    }
    [self.rightButton removeFromSuperview];
}

-(UIImageView *)titleImageView{
    if (!_titleImageView) {
        _titleImageView = [[UIImageView alloc]init];
        _titleImageView.contentMode = UIViewContentModeScaleAspectFit;
    }
    return _titleImageView;
}

-(UILabel *)titleLabel{
    if (!_titleLabel) {
        _titleLabel = [[UILabel alloc]init];
        _titleLabel.backgroundColor = [UIColor clearColor];
        _titleLabel.font = [UIFont systemFontOfSize:19];
        _titleLabel.textColor = [UIColor yj_darkTextColor];
        _titleLabel.textAlignment = NSTextAlignmentCenter;
    }
    return _titleLabel;
}

-(UIView *)bottomLine{
    if (!_bottomLine) {
        _bottomLine = [[UIView alloc]init];
        _bottomLine.backgroundColor = [UIColor yj_lineColor];
    }
    return _bottomLine;
}

- (void)damNavigationBarDidScrollWithScrollView:(UIScrollView *)scrollView{
    CGFloat offsetY = 1 - scrollView.contentOffset.y/100;
    self.titleLabel.textColor = [[UIColor yj_darkTextColor]colorWithAlphaComponent:offsetY];
}

@end
