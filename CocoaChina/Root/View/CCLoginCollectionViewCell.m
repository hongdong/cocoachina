//
//  YJLoginCollectionViewCell.m
//  yuedu
//
//  Created by Zhang on 15/10/13.
//  Copyright © 2015年 北京易利友信息技术有限公司. All rights reserved.
//

#import "CCLoginCollectionViewCell.h"

@interface CCLoginCollectionViewCell ()
@property (nonatomic, strong) UIImageView *imageView;
@property (nonatomic, strong) UILabel *textLabel;
@end

@implementation CCLoginCollectionViewCell

-(instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        [self commonInit];
    }
    return self;
}

-(void)commonInit{
    self.backgroundColor = [UIColor whiteColor];
    [self.contentView addSubview:self.textLabel];
    [self.contentView addSubview:self.imageView];
    
    [self.imageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.contentView.mas_centerY).offset(-20);
        make.centerX.equalTo(self.contentView.mas_centerX);
        make.size.mas_equalTo(CGSizeMake(50, 50));
    }];
    
    [self.textLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.contentView.mas_centerX);
        make.top.equalTo(self.imageView.mas_bottom).offset(10);
    }];
}

-(void)setTitle:(NSString *)title{
    _title = [title copy];
    self.textLabel.text = _title;
}

-(void)setImage:(UIImage *)image{
    _image = image;
    self.imageView.image = image;
}

#pragma mark - getters and setters

-(UILabel *)textLabel{
    if (!_textLabel) {
        _textLabel = [[UILabel alloc]init];
        _textLabel.backgroundColor = [UIColor clearColor];
        _textLabel.font = [UIFont systemFontOfSize:14];
        _textLabel.textColor = [UIColor yj_lightTextColor];
    }
    return _textLabel;
}

-(UIImageView *)imageView{
    if (!_imageView) {
        _imageView = [[UIImageView alloc]init];
        _imageView.contentMode = UIViewContentModeScaleAspectFit;
    }
    return _imageView;
}

@end
