//
//  YJRootViewController.m
//  yuedu
//
//  Created by Zhang on 15/9/16.
//  Copyright © 2015年 北京易利友信息技术有限公司. All rights reserved.
//

#import "CCRootViewController.h"
#import "CCChannelView.h"
#import "CCChannelManager.h"
#import "CCNavigationBar.h"
#import "CCSetViewController.h"
#import <GoogleMobileAds/GoogleMobileAds.h>
#import "CCMasterViewController.h"

@interface CCRootViewController ()<DAMChannelViewDataSource,DAMChannelViewDelegate,UIScrollViewDelegate,GADInterstitialDelegate>{
    NSArray *_subViewControllers;
    UIView *_containerView;
}
@property (nonatomic, strong) CCNavigationBar *navBar;
@property (nonatomic, strong) CCChannelView *channelView;
@property (nonatomic, strong) UIScrollView *mainScrollView;
@property (nonatomic, strong) UIView *containerView;
@property (nonatomic, assign) CGFloat lastOffsetX;
@property (nonatomic, strong) NSMutableDictionary *trimDictionary;
@property (nonatomic, strong) NSArray *viewControllers;
@property (nonatomic, assign) NSInteger currentPage;
@property (nonatomic, strong) CCMasterViewController *currentViewController;
@property (nonatomic, strong) CCMasterViewController *nextViewController;
@property (nonatomic, copy) NSArray *channels;
@property (nonatomic, strong) UIButton *menuButton;
@property (nonatomic, strong) UIView *leftHidenView;
@property (nonatomic, strong) GADInterstitial *interstitial;
@property (nonatomic, strong) NSString *originUserAgent;
@end

@implementation CCRootViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.fd_prefersNavigationBarHidden = YES;
    self.edgesForExtendedLayout = UIRectEdgeNone;
    [self commonInit];
    [self.channelView reloadData];
    //self.interstitial = [self createAndLoadInterstitial];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(viewDidBecomeActive:) name:UIApplicationWillEnterForegroundNotification object:nil];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    if(self.originUserAgent==nil){
        UIWebView* webView = [[UIWebView alloc] initWithFrame:CGRectZero];
        self.originUserAgent = [webView stringByEvaluatingJavaScriptFromString:@"navigator.userAgent"];
    }
    NSString *ua=[self.originUserAgent stringByAppendingFormat:@" CocoaChina/1.0"];
    ua = [ua stringByReplacingOccurrencesOfString:@"iPhone" withString:@"iPad"];
    [[NSUserDefaults standardUserDefaults]registerDefaults:@{@"UserAgent":ua}];
}

-(void)viewDidBecomeActive:(NSNotification *)notification{
    if (self.interstitial.isReady && self.navigationController.topViewController == self) {
        [self.interstitial presentFromRootViewController:self];
    }
}

-(void)commonInit{
    self.view.backgroundColor = [UIColor yj_cellBackgroundColor];
    [self.view addSubview:self.mainScrollView];
    [self.view addSubview:self.leftHidenView];
    [self.view addSubview:self.channelView];
    [self.view addSubview:self.navBar];
    [self.mainScrollView addSubview:self.containerView];
    
    [self.navBar mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.right.equalTo(self.view);
        make.height.equalTo(@64);
    }];
    [self.containerView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.mainScrollView);
        make.height.equalTo(self.mainScrollView);
    }];
    
    [self.channelView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(self.view);
        make.top.equalTo(self.navBar.mas_bottom);
        make.height.equalTo(@40);
    }];
    [self.mainScrollView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.bottom.equalTo(self.view);
        make.top.equalTo(self.channelView.mas_bottom);
    }];
}

#pragma mark - UIScrollViewDelegate

-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    [self.channelView channelViewDidEndDecelerating:scrollView];
    NSInteger page = floor(scrollView.contentOffset.x/SCREEN_WIDTH);
    if (page < 0 && page > self.channels.count) {
        return;
    }
    if (page != self.currentPage) {
        self.currentPage = page;
        [self.currentViewController willMoveToParentViewController:nil];
        [self.currentViewController beginAppearanceTransition:NO animated:YES];
        [self.currentViewController.view removeFromSuperview];
        [self.currentViewController endAppearanceTransition];
        [self.currentViewController removeFromParentViewController];
        self.currentViewController = self.nextViewController;
        self.nextViewController = nil;
    }else{
        [self.nextViewController willMoveToParentViewController:nil];
        [self.nextViewController beginAppearanceTransition:NO animated:YES];
        [self.nextViewController.view removeFromSuperview];
        [self.nextViewController endAppearanceTransition];
        [self.nextViewController removeFromParentViewController];
        self.nextViewController = nil;
    }
}

-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
    [self.channelView channelViewDidScroll:scrollView];
    NSInteger page;
    CGFloat currentOffsetX = scrollView.contentOffset.x;
    if (_lastOffsetX > currentOffsetX) {
        //向左滑动
        page = floor(scrollView.contentOffset.x/SCREEN_WIDTH);
    }else{
        //向右滑动
        page = ceil(scrollView.contentOffset.x/SCREEN_WIDTH);
    }
    _lastOffsetX = currentOffsetX;
    
    if(page >=0 && page < self.viewControllers.count){
        CCMasterViewController *newController = [self insertControllersAtIndex:page];
        if(self.nextViewController == newController){
            return;
        }
        if (page == self.currentPage) {
            return;
        }
        self.nextViewController = newController;
        [self addChildViewController:self.nextViewController];
        [self.nextViewController beginAppearanceTransition:YES animated:NO];
        CGRect baseFrame = self.mainScrollView.bounds;
        baseFrame.origin.x = SCREEN_WIDTH*page;
        self.nextViewController.view.frame = baseFrame;
        self.nextViewController.view.autoresizingMask = UIViewAutoresizingFlexibleHeight|UIViewAutoresizingFlexibleWidth;
        [self.containerView addSubview:self.nextViewController.view];
        [self.nextViewController endAppearanceTransition];
        [self.nextViewController didMoveToParentViewController:self];
    }
    
}

-(CCMasterViewController *)insertControllersAtIndex:(NSInteger)index{
    CCChannelModel *channelModel = self.channels[index];
    CCMasterViewController *childController;
    if (!self.trimDictionary[channelModel.channelId]) {
        childController = self.viewControllers[index];
        self.trimDictionary[channelModel.channelId] = childController;
    }else{
        childController = self.trimDictionary[channelModel.channelId];
    }
    return childController;
}

#pragma mark - DAMChannelViewDataSource
-(NSInteger)numberOfChannelsForChannelView:(CCChannelView *)channelView{
    
    return self.channels.count;
}

-(NSString *)titleForChannelForChannelView:(CCChannelView *)channelView atIndex:(NSInteger)index{
    
    CCChannelModel *channelModel = self.channels[index];
    return channelModel.channelName;
}

#pragma mark - DAMChannelViewDelegate

-(void)channelView:(CCChannelView *)channelView didSelectedItemAtIndex:(NSInteger)index{
    
    CGRect rect = self.mainScrollView.bounds;
    rect.origin.x = index * SCREEN_WIDTH;
    [self.mainScrollView scrollRectToVisible:rect animated:NO];
    [self scrollViewDidEndDecelerating:self.mainScrollView];
}

#pragma mark - GADInterstitialDelegate

- (void)interstitialDidDismissScreen:(GADInterstitial *)interstitial {
    self.interstitial = [self createAndLoadInterstitial];
}

-(void)interstitialDidReceiveAd:(GADInterstitial *)ad{
    
}

#pragma mark - Action

-(void)onMenuButtonClicked:(UIButton *)sender{
    CCSetViewController *setViewController = [[CCSetViewController alloc]init];
    [self showViewController:setViewController sender:self];
}

#pragma mark - getters and setters
- (GADInterstitial *)createAndLoadInterstitial {
    GADInterstitial *interstitial = [[GADInterstitial alloc] initWithAdUnitID:@"ca-app-pub-6979762357915263/6407798838"];
    interstitial.delegate = self;
    [interstitial loadRequest:[GADRequest request]];
    return interstitial;
}

-(NSMutableDictionary *)trimDictionary{
    if (!_trimDictionary) {
        _trimDictionary = [[NSMutableDictionary alloc]init];
    }
    return _trimDictionary;
}

-(NSArray *)channels{
    if (!_channels) {
        _channels = [[CCChannelManager sharedManager].confirmedChannels copy];
        NSMutableArray *viewControllers = [[NSMutableArray alloc]init];
        for (CCChannelModel *channelModel in _channels) {
            CCMasterViewController *newsBaseController = [[CCMasterViewController alloc]initWithChannelModel:channelModel];
            [viewControllers addObject:newsBaseController];
        }
        self.viewControllers = viewControllers;
    }
    return _channels;
}

-(void)setViewControllers:(NSArray *)viewControllers{
    _viewControllers = viewControllers;
    [self.containerView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.equalTo(@(SCREEN_WIDTH*viewControllers.count));
    }];
    [self.view layoutIfNeeded];
    ///第一次设置 或者 不包含当前控制器
    if (self.currentViewController != nil && ![_viewControllers containsObject:self.currentViewController]) {
        [self.currentViewController willMoveToParentViewController:nil];
        [self.currentViewController beginAppearanceTransition:NO animated:NO];
        [self.currentViewController.view removeFromSuperview];
        [self.currentViewController endAppearanceTransition];
        [self.currentViewController removeFromParentViewController];
        self.currentViewController = nil;
    }
    
    if (self.currentViewController == nil) {
        NSInteger page = 0;
        CCMasterViewController *newsController = [self insertControllersAtIndex:page];
        [self addChildViewController:newsController];
        [newsController beginAppearanceTransition:YES animated:NO];
        CGRect baseFrame = self.mainScrollView.bounds;
        baseFrame.origin.x = SCREEN_WIDTH*page;
        baseFrame.size.width = SCREEN_WIDTH;
        [self.mainScrollView scrollRectToVisible:baseFrame animated:NO];
        newsController.view.frame = baseFrame;
        newsController.view.autoresizingMask = UIViewAutoresizingFlexibleHeight|UIViewAutoresizingFlexibleWidth;
        [self.containerView addSubview:newsController.view];
        [newsController endAppearanceTransition];
        [newsController didMoveToParentViewController:self];
        self.currentViewController = newsController;
    }else if([_viewControllers containsObject:self.currentViewController]){
        NSInteger page = [_viewControllers indexOfObject:self.currentViewController];
        CGRect baseFrame = self.mainScrollView.bounds;
        baseFrame.origin.x = SCREEN_WIDTH*page;
        baseFrame.size.width = SCREEN_WIDTH;
        self.currentViewController.view.frame = baseFrame;
        [self.mainScrollView scrollRectToVisible:baseFrame animated:NO];
    }
}

-(void)setCurrentPage:(NSInteger)currentPage{
    if (currentPage<0 || currentPage>=self.viewControllers.count) {
        return;
    }
    _currentPage = currentPage;
}

-(void)setCurrentPage:(NSInteger)currentPage animate:(BOOL)animate{
    self.currentPage = currentPage;
    [self.mainScrollView scrollRectToVisible:CGRectMake(SCREEN_WIDTH*currentPage, 0, SCREEN_WIDTH,CGRectGetHeight(self.mainScrollView.frame)) animated:animate];
}

-(void)setCurrentViewController:(CCMasterViewController *)currentViewController{
    _currentViewController.scrollToTop = NO;
    _currentViewController = currentViewController;
    _currentViewController.scrollToTop = YES;
    _currentViewController.firstLoad = YES;
}

-(CCChannelView *)channelView{
    if (!_channelView) {
        _channelView = [[CCChannelView alloc]init];
        _channelView.delegate = self;
        _channelView.dataSource = self;
    }
    return _channelView;
}

-(UIScrollView *)mainScrollView{
    if (!_mainScrollView) {
        _mainScrollView = [[UIScrollView alloc]init];
        _mainScrollView.delegate = self;
        _mainScrollView.pagingEnabled = YES;
        _mainScrollView.scrollsToTop = NO;
        _mainScrollView.showsHorizontalScrollIndicator = NO;
        _mainScrollView.showsVerticalScrollIndicator = NO;
    }
    return _mainScrollView;
}

-(UIView *)containerView{
    if (!_containerView) {
        _containerView = [[UIView alloc]init];
    }
    return _containerView;
}

-(CCNavigationBar *)navBar{
    if (!_navBar) {
        _navBar = [[CCNavigationBar alloc]init];
        _navBar.titleImage = [UIImage imageNamed:@"nav_logo"];
        _navBar.rightButton = self.menuButton;
        _navBar.backgroundColor = [UIColor yj_cellBackgroundColor];
    }
    return _navBar;
}

-(UIButton *)menuButton{
    if (!_menuButton) {
        _menuButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [_menuButton addTarget:self action:@selector(onMenuButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
        _menuButton.frame = CGRectMake(0, 0, 45, 44);
        [_menuButton setBackgroundImage:[UIImage imageNamed:@"nav_menu"] forState:UIControlStateNormal];
    }
    return _menuButton;
}

#pragma mark - overwirte

- (NSArray *)childViewControllersWithAppearanceCallbackAutoForward{
    if (self.currentViewController == nil) {
        return nil;
    }
    return @[self.currentViewController];
}

- (NSArray *)childViewControllersWithRotationCallbackAutoForward{
    if (self.currentViewController == nil) {
        return nil;
    }
    return @[self.currentViewController];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - GC

-(void)dealloc{
    [[NSNotificationCenter defaultCenter]removeObserver:self];
}

@end