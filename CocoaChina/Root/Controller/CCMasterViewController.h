//
//  CCMasterViewController.h
//  CocoaChina
//
//  Created by Zhang on 15/10/30.
//  Copyright © 2015年 北京大米信息技术有限公司. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CCChannelModel.h"

@interface CCMasterViewController : UIViewController
@property (nonatomic, copy) CCChannelModel *channelModel;
@property (nonatomic, assign) BOOL scrollToTop;
@property (nonatomic, assign) BOOL firstLoad;
-(instancetype)initWithChannelModel:(CCChannelModel *)channelModel;
@end
