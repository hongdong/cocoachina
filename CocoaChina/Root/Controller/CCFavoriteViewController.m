//
//  CCFavoriteViewController.m
//  CocoaChina
//
//  Created by Zhang on 15/10/31.
//  Copyright © 2015年 北京大米信息技术有限公司. All rights reserved.
//

#import "CCFavoriteViewController.h"
#import "UIScrollView+EmptyDataSet.h"
#import <MJRefresh/MJRefresh.h>
#import "CCHistoryModel.h"
#import "CCNavigationBar.h"
#import "CCMasterTableViewCell.h"
#import "CCWebViewController.h"
#import "CCFavoriteService.h"
#import "CCRefreshHeader.h"

@interface CCFavoriteViewController ()<UITableViewDataSource,UITableViewDelegate,DZNEmptyDataSetDelegate,DZNEmptyDataSetSource,MGSwipeTableCellDelegate>
@property (nonatomic, strong) CCNavigationBar *navBar;
@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) NSMutableArray *dataSource;
@property (nonatomic, assign) NSInteger limit;
@property (nonatomic, assign) NSInteger skip;
@property (nonatomic, assign,getter=isRefreshing) BOOL refreshing;
@property (nonatomic, strong) UIButton *backButton;
@end

@implementation CCFavoriteViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.fd_prefersNavigationBarHidden = YES;
    self.edgesForExtendedLayout = UIRectEdgeNone;
    [self commonInit];
    
    self.limit = 30;
    self.skip = 0;
    self.dataSource = [[NSMutableArray alloc]init];
    
    __weak typeof(self) weakSelf = self;
    MJRefreshNormalHeader *header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        weakSelf.skip = 0;
        weakSelf.refreshing = YES;
        [weakSelf getData];
    }];
    header.lastUpdatedTimeLabel.font = [UIFont systemFontOfSize:13];
    header.lastUpdatedTimeLabel.textColor = [UIColor yj_lightTextColor];
    header.stateLabel.font = [UIFont systemFontOfSize:13];
    header.stateLabel.textColor = [UIColor yj_lightTextColor];
//    header.lastUpdatedTimeLabel.hidden = YES;
//    header.stateLabel.hidden = YES;
//    [header setImages:@[
//                        [UIImage imageNamed:@"load_1"],
//                        [UIImage imageNamed:@"load_2"],
//                        [UIImage imageNamed:@"load_3"],
//                        [UIImage imageNamed:@"load_4"]
//                        ] forState:MJRefreshStateRefreshing];
//    [header setImages:@[
//                        [UIImage imageNamed:@"load_1"]
//                        ] forState:MJRefreshStatePulling];
//    [header setImages:@[
//                        [UIImage imageNamed:@"load_1"]
//                        ] forState:MJRefreshStateWillRefresh];
//    [header setImages:@[
//                        [UIImage imageNamed:@"load_1"]
//                        ] forState:MJRefreshStateIdle];
    self.tableView.mj_header = header;
    
    MJRefreshAutoStateFooter *footer = [MJRefreshAutoStateFooter footerWithRefreshingBlock:^{
        weakSelf.refreshing = NO;
        weakSelf.skip += weakSelf.limit;
        [weakSelf getData];
    }];
    footer.triggerAutomaticallyRefreshPercent = -20;
    footer.stateLabel.font = [UIFont systemFontOfSize:13];
    footer.stateLabel.textColor = [UIColor yj_lightTextColor];
    [footer setTitle:@"没有更多了" forState:MJRefreshStateNoMoreData];
    self.tableView.mj_footer = footer;
    [self.tableView.mj_header beginRefreshing];
}

-(void)commonInit{
    self.view.backgroundColor = [UIColor yj_cellBackgroundColor];
    [self.view addSubview:self.tableView];
    [self.view addSubview:self.navBar];
    
    [self.navBar mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.right.equalTo(self.view);
        make.height.equalTo(@64);
    }];
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.view).insets(UIEdgeInsetsMake(64, 0, 0, 0));
    }];
}

#pragma mark - data
-(void)getData{
    __weak CCFavoriteViewController *weakSelf = self;
    BmobQuery *favoriteQuery = [BmobQuery queryWithClassName:@"favorite"];
    favoriteQuery.cachePolicy = kBmobCachePolicyNetworkOnly;
    favoriteQuery.limit = self.limit;
    favoriteQuery.skip = self.skip;//self.dataSource.count;
    favoriteQuery.maxCacheAge = 60*60*24*7;
    favoriteQuery.isGroupcount = YES;
    [favoriteQuery orderByDescending:@"createdAt"];
    [favoriteQuery whereKey:@"uid" equalTo:[BmobUser getCurrentUser].objectId];
    [favoriteQuery findObjectsInBackgroundWithBlock:^(NSArray *array, NSError *error) {
        if (weakSelf.isRefreshing) {
            [weakSelf.dataSource removeAllObjects];
        }
        for (BmobObject *favoriteObject in array) {
            NSDictionary *masterDictionary = [favoriteObject objectForKey:@"newsData"];
            CCMasterModel *masterModel = [[CCMasterModel alloc]initWithDictionary:masterDictionary];
            masterModel.objectId = favoriteObject.objectId;
            if (![weakSelf.dataSource containsObject:masterModel]) {
                [weakSelf.dataSource addObject:masterModel];
            }
        }
        
        if (weakSelf.isRefreshing) {
            [weakSelf.tableView.mj_header endRefreshing];
            weakSelf.refreshing = NO;
        }else{
            [weakSelf.tableView.mj_footer endRefreshing];
        }
        if (array.count < weakSelf.limit) {
            [weakSelf.tableView.mj_footer endRefreshingWithNoMoreData];
        }
        [weakSelf.tableView reloadData];
    }];
}

#pragma mark - UIScrollViewDelelgate
-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
    
}
#pragma mark - UITableViewDataSource
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return self.dataSource.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    CCMasterTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kMasterCellIdentifier forIndexPath:indexPath];
    CCMasterModel *masterCellModel = self.dataSource[indexPath.row];
    cell.model = masterCellModel;
    cell.rightSwipeSettings.transition = MGSwipeStateSwippingLeftToRight;
    cell.delegate = self;
    return cell;
}

#pragma mark - UITableViewDelegate

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    CCMasterModel *masterCellModel = self.dataSource[indexPath.row];
    CCMasterTableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    cell.model = masterCellModel;
    
    CCWebViewController *webViewController = [[CCWebViewController alloc]init];
    webViewController.masterModel = masterCellModel;
    [self showViewController:webViewController sender:self];
}

#pragma mark - MGSwipeTableCellDelegate

-(BOOL) swipeTableCell:(MGSwipeTableCell*) cell canSwipe:(MGSwipeDirection) direction{
    
    return direction == MGSwipeDirectionRightToLeft;
}
-(void) swipeTableCell:(MGSwipeTableCell*) cell didChangeSwipeState:(MGSwipeState) state gestureIsActive:(BOOL) gestureIsActive{
    
}
-(BOOL) swipeTableCell:(MGSwipeTableCell*) cell tappedButtonAtIndex:(NSInteger) index direction:(MGSwipeDirection)direction fromExpansion:(BOOL) fromExpansion{
    return YES;
}

-(NSArray*) swipeTableCell:(MGSwipeTableCell*) cell swipeButtonsForDirection:(MGSwipeDirection)direction
             swipeSettings:(MGSwipeSettings*) swipeSettings expansionSettings:(MGSwipeExpansionSettings*) expansionSettings{
    swipeSettings.transition = MGSwipeTransitionBorder;
    expansionSettings.buttonIndex = 0;
    expansionSettings.fillOnTrigger = YES;
    expansionSettings.threshold = 1.1;
    
    __weak CCFavoriteViewController * weakSelf = self;
    if (direction == MGSwipeDirectionRightToLeft) {
        return @[[MGSwipeButton buttonWithTitle:@"取消收藏" backgroundColor:[UIColor yj_blueColor] padding:15 callback:^BOOL(MGSwipeTableCell *sender) {
            
            NSIndexPath * indexPath = [weakSelf.tableView indexPathForCell:sender];
            CCMasterModel *masterModel =  weakSelf.dataSource[indexPath.row];
            BmobObject *bmobObject = [BmobObject objectWithoutDatatWithClassName:@"favorite"  objectId:masterModel.objectId];
            [bmobObject deleteInBackgroundWithBlock:^(BOOL isSuccessful, NSError *error) {
                if (isSuccessful) {
                    [JDStatusBarNotification showWithStatus:@"取消收藏成功" dismissAfter:2 styleName:JDStatusBarStyleSuccess];
                } else if (error){
                    [JDStatusBarNotification showWithStatus:@"取消收藏失败,过一会儿再试吧" dismissAfter:2 styleName:JDStatusBarStyleError];
                } else{
                    [JDStatusBarNotification showWithStatus:@"服务器好像出了点小问题~" dismissAfter:2 styleName:JDStatusBarStyleError];
                }
            }];
            [weakSelf.dataSource removeObjectAtIndex:indexPath.row];
            [weakSelf.tableView reloadData];
            return NO;
        }]];
    }
    return nil;
}


#pragma mark - DZNEmptyDataSetDelegate
-(BOOL)emptyDataSetShouldDisplay:(UIScrollView *)scrollView{
    return self.dataSource.count == 0;
}
-(BOOL)emptyDataSetShouldAllowScroll:(UIScrollView *)scrollView{
    return YES;
}
-(NSAttributedString *)titleForEmptyDataSet:(UIScrollView *)scrollView{
    
    return [[NSAttributedString alloc]initWithString:@"没有收藏过任何文章" attributes:@{NSForegroundColorAttributeName:[UIColor yj_lightTextColor],NSFontAttributeName:[UIFont systemFontOfSize:15]}];
}

-(UIImage *)imageForEmptyDataSet:(UIScrollView *)scrollView{
    
    return [UIImage imageNamed:@"not_found"];
}

-(CGFloat)verticalOffsetForEmptyDataSet:(UIScrollView *)scrollView{
    
    return -80;
}

#pragma mark - DZNEmptyDataSetSource

#pragma mark - action
-(void)onBackButtonClicked:(UIButton *)sender{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - getters and setters

-(CCNavigationBar *)navBar{
    if (!_navBar) {
        _navBar = [[CCNavigationBar alloc]init];
        _navBar.title = @"我的收藏";
        _navBar.leftButton = self.backButton;
    }
    return _navBar;
}

-(UIButton *)backButton{
    if (!_backButton) {
        _backButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [_backButton addTarget:self action:@selector(onBackButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
        [_backButton setBackgroundImage:[UIImage imageNamed:@"nav_back"] forState:UIControlStateNormal];
    }
    return _backButton;
}

-(UITableView *)tableView{
    if (!_tableView) {
        _tableView = [[UITableView alloc]initWithFrame:CGRectZero style:UITableViewStylePlain];
        [_tableView registerClass:[CCMasterTableViewCell class] forCellReuseIdentifier:kMasterCellIdentifier];
        _tableView.backgroundColor = [UIColor yj_cellBackgroundColor];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.rowHeight = 90;
        _tableView.emptyDataSetDelegate = self;
        _tableView.emptyDataSetSource = self;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    }
    return _tableView;
}

-(NSMutableArray *)dataSource{
    if (!_dataSource) {
        _dataSource = [[NSMutableArray alloc]init];
    }
    return _dataSource;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)dealloc{
    
}

@end
