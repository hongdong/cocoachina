//
//  YJRegistViewController.m
//  yuedu
//
//  Created by Zhang on 15/10/14.
//  Copyright © 2015年 北京易利友信息技术有限公司. All rights reserved.
//

#import "CCRegistViewController.h"
#import "CCNavigationBar.h"

@interface CCRegistViewController ()<UITextFieldDelegate,UIScrollViewDelegate>
@property (nonatomic, strong) CCNavigationBar *navBar;
@property (nonatomic, strong) UIButton *backButton;
@property (nonatomic, strong) UIScrollView *mainScrollView;
@property (nonatomic, strong) UIView *contentView;
///登录
@property (nonatomic, strong) UIImageView *userImageView;
@property (nonatomic, strong) UIImageView *passwdImageView;
@property (nonatomic, strong) UIImageView *validateCodeImageView;
@property (nonatomic, strong) UITextField *usernameField;
@property (nonatomic, strong) UITextField *passwordTextField;
@property (nonatomic, strong) UITextField *validateCodeField;
@property (nonatomic, strong) UIButton *validateCodeButton;
@property (nonatomic, strong) UIView *userLine;
@property (nonatomic, strong) UIView *passwordLine;
@property (nonatomic, strong) UIView *validateCodeLine;
@property (nonatomic, strong) UIButton *reigstButton;
@end

@implementation CCRegistViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.fd_prefersNavigationBarHidden = YES;
    [self commonInit];
}

-(void)commonInit{
    self.edgesForExtendedLayout = UIRectEdgeNone;
    self.view.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:self.mainScrollView];
    [self.view addSubview:self.navBar];
    [self.mainScrollView addSubview:self.contentView];
    //用户名
    [self.contentView addSubview:self.userImageView];
    [self.contentView addSubview:self.usernameField];
    [self.contentView addSubview:self.userLine];
    ///验证码
    [self.contentView addSubview:self.validateCodeImageView];
    [self.contentView addSubview:self.validateCodeField];
    [self.contentView addSubview:self.validateCodeButton];
    [self.contentView addSubview:self.validateCodeLine];
    //密码
    [self.contentView addSubview:self.passwdImageView];
    [self.contentView addSubview:self.passwordTextField];
    [self.contentView addSubview:self.passwordLine];
    [self.contentView addSubview:self.reigstButton];
    
    [self.navBar mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.right.equalTo(self.view);
        make.height.equalTo(@64);
    }];
    
    [self.mainScrollView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.left.bottom.equalTo(self.view);
        make.top.equalTo(self.navBar.mas_bottom);
    }];
    
    [self.contentView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.mainScrollView);
        make.width.equalTo(self.mainScrollView);
    }];
    
    
    [self.userImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.contentView.mas_top).offset(30);
        make.left.equalTo(self.contentView.mas_left).offset(15);
        make.size.mas_equalTo(CGSizeMake(20, 20));
    }];
    
    [self.usernameField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.userImageView);
        make.left.equalTo(self.userImageView.mas_right).offset(8);
        make.right.equalTo(self.contentView.mas_right).offset(-15);
        make.height.equalTo(@30);
    }];
    
    [self.userLine mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.contentView.mas_left).offset(15);
        make.right.equalTo(self.contentView.mas_right).offset(-15);
        make.height.equalTo(@0.5);
        make.top.equalTo(self.userImageView.mas_bottom).offset(10);
    }];
    
    [self.passwdImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.userLine.mas_bottom).offset(10);
        make.left.equalTo(self.contentView.mas_left).offset(15);
        make.size.mas_equalTo(CGSizeMake(20, 20));
    }];
    
    [self.passwordTextField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.passwdImageView);
        make.left.equalTo(self.passwdImageView.mas_right).offset(8);
        make.right.equalTo(self.contentView.mas_right).offset(-15);
        make.height.equalTo(@30);
    }];
    
    [self.passwordLine mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.contentView.mas_left).offset(15);
        make.right.equalTo(self.contentView.mas_right).offset(-15);
        make.height.equalTo(@0.5);
        make.top.equalTo(self.passwdImageView.mas_bottom).offset(10);
    }];
    
    [self.validateCodeImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.passwordLine.mas_top).offset(15);
        make.left.equalTo(self.contentView.mas_left).offset(15);
        make.size.mas_equalTo(CGSizeMake(20, 20));
    }];
    
    [self.validateCodeField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.validateCodeImageView);
        make.left.equalTo(self.validateCodeImageView.mas_right).offset(8);
        make.height.equalTo(@30);
    }];
    
    [self.validateCodeButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.validateCodeImageView.mas_centerY);
        make.left.equalTo(self.validateCodeField.mas_right).offset(15);
        make.right.equalTo(self.contentView.mas_right).offset(-15);
        make.size.mas_equalTo(CGSizeMake(100, 40));
    }];
    
    [self.validateCodeLine mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.validateCodeImageView.mas_bottom).offset(15);
        make.left.equalTo(self.contentView.mas_left).offset(15);
        make.right.equalTo(self.contentView.mas_right).offset(-15);
        make.height.equalTo(@0.5);
    }];
    
    [self.reigstButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.validateCodeLine.mas_bottom).offset(15);
        make.left.equalTo(self.contentView.mas_left).offset(15);
        make.right.equalTo(self.contentView.mas_right).offset(-15);
        make.height.equalTo(@44);
        make.bottom.equalTo(self.contentView.mas_bottom);
    }];
}

#pragma mark - UIScrollViewDelegate
-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
    if (scrollView.isDragging) {
        [self.view endEditing:YES];
    }
}

#pragma mark - UITextFieldDelegate
-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    if (textField == self.usernameField) {
        [self.passwordTextField becomeFirstResponder];
    }
    if (textField == self.passwordTextField) {
        [self.validateCodeField becomeFirstResponder];
    }
    if (textField == self.validateCodeField) {
        [self.usernameField becomeFirstResponder];
    }
    return YES;
}

#pragma mark - actions
-(void)onBackButtonClicked:(UIButton *)sender{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)onRegistButtonClicked:(UIButton *)sender{
    
}

-(void)onValidateButtonClicked:(UIButton *)sender{
    //获取验证码
    BmobUser *user = [[BmobUser alloc]init];
    user.mobilePhoneNumber = self.usernameField.text;
    
    
    [BmobSMS requestSMSCodeInBackgroundWithPhoneNumber:self.usernameField.text andTemplate:@"注册模板" resultBlock:^(int number, NSError *error) {
       
        if (!error) {
            
        }else{
         
        }
    }];
}

#pragma mark - getters and setters
-(CCNavigationBar *)navBar{
    if (!_navBar) {
        _navBar = [[CCNavigationBar alloc]init];
        _navBar.leftButton = self.backButton;
        _navBar.title = @"注册阅记账号";
    }
    return _navBar;
}

-(UIButton *)backButton{
    if (!_backButton) {
        _backButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [_backButton addTarget:self action:@selector(onBackButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
        [_backButton setBackgroundImage:[UIImage imageNamed:@"nav_back"] forState:UIControlStateNormal];
    }
    return _backButton;
}

-(UIScrollView *)mainScrollView{
    if (!_mainScrollView) {
        _mainScrollView = [[UIScrollView alloc]init];
        _mainScrollView.alwaysBounceVertical = YES;
        _mainScrollView.delegate = self;
    }
    return _mainScrollView;
}

-(UIView *)contentView{
    if (!_contentView) {
        _contentView = [[UIView alloc]init];
        _contentView.backgroundColor = [UIColor whiteColor];
    }
    return _contentView;
}

-(UIImageView *)userImageView{
    if (!_userImageView) {
        _userImageView = [[UIImageView alloc]init];
        _userImageView.image = [UIImage imageNamed:@"login_username_icon"];
    }
    return _userImageView;
}

-(UIImageView *)passwdImageView{
    if (!_passwdImageView) {
        _passwdImageView = [[UIImageView alloc]init];
        _passwdImageView.image = [UIImage imageNamed:@"login_password_icon"];
    }
    return _passwdImageView;
}

-(UIImageView *)validateCodeImageView{
    if (!_validateCodeImageView) {
        _validateCodeImageView = [[UIImageView alloc]init];
        _validateCodeImageView.image = [UIImage imageNamed:@"login_password_icon"];
    }
    return _validateCodeImageView;
}

-(UITextField *)usernameField{
    if (!_usernameField) {
        _usernameField = [[UITextField alloc]init];
        _usernameField.returnKeyType = UIReturnKeyNext;
        _usernameField.font = [UIFont systemFontOfSize:15];
        _usernameField.attributedPlaceholder = [[NSAttributedString alloc]initWithString:@"手机号" attributes:@{NSForegroundColorAttributeName:[UIColor yj_lightTextColor]}];
        _usernameField.clearButtonMode = UITextFieldViewModeWhileEditing;
        _usernameField.delegate = self;
        _usernameField.keyboardType = UIKeyboardTypePhonePad;
    }
    return _usernameField;
}

-(UITextField *)passwordTextField{
    if (!_passwordTextField) {
        _passwordTextField = [[UITextField alloc]init];
        _passwordTextField.returnKeyType = UIReturnKeyNext;
        _passwordTextField.font = [UIFont systemFontOfSize:15];
        _passwordTextField.attributedPlaceholder = [[NSAttributedString alloc]initWithString:@"密码" attributes:@{NSForegroundColorAttributeName:[UIColor yj_lightTextColor]}];
        _passwordTextField.clearButtonMode = UITextFieldViewModeWhileEditing;
        _passwordTextField.secureTextEntry = YES;
        _passwordTextField.delegate = self;
    }
    return _passwordTextField;
}

-(UITextField *)validateCodeField{
    if (!_validateCodeField) {
        _validateCodeField = [[UITextField alloc]init];
        _validateCodeField.returnKeyType = UIReturnKeyNext;
        _validateCodeField.font = [UIFont systemFontOfSize:15];
        _validateCodeField.attributedPlaceholder = [[NSAttributedString alloc]initWithString:@"验证码" attributes:@{NSForegroundColorAttributeName:[UIColor yj_lightTextColor]}];
        _validateCodeField.clearButtonMode = UITextFieldViewModeWhileEditing;
        _validateCodeField.delegate = self;
    }
    return _validateCodeField;
}

-(UIView *)userLine{
    if (!_userLine) {
        _userLine = [[UIView alloc]init];
        _userLine.backgroundColor = [UIColor yj_lineColor];
    }
    return _userLine;
}

-(UIView *)passwordLine{
    if (!_passwordLine) {
        _passwordLine = [[UIView alloc]init];
        _passwordLine.backgroundColor = [UIColor yj_lineColor];
    }
    return _passwordLine;
}

-(UIView *)validateCodeLine{
    if (!_validateCodeLine) {
        _validateCodeLine = [[UIView alloc]init];
        _validateCodeLine.backgroundColor = [UIColor yj_lineColor];
    }
    return _validateCodeLine;
}

-(UIButton *)validateCodeButton{
    if (!_validateCodeButton) {
        _validateCodeButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _validateCodeButton.titleLabel.font = [UIFont systemFontOfSize:15];
        [_validateCodeButton addTarget:self action:@selector(onValidateButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
        [_validateCodeButton setTitle:@"获取验证码" forState:UIControlStateNormal];
        [_validateCodeButton setTitleColor:[UIColor yj_darkTextColor] forState:UIControlStateNormal];
        [_validateCodeButton setTitleColor:[UIColor yj_lightTextColor] forState:UIControlStateDisabled];
        _validateCodeButton.layer.cornerRadius = 5;
        _validateCodeButton.layer.borderWidth = 0.5;
        _validateCodeButton.layer.borderColor = [UIColor yj_lineColor].CGColor;
        _validateCodeButton.layer.masksToBounds = YES;
        [_validateCodeButton setBackgroundImage:[UIImage imageWithColor:[UIColor whiteColor]] forState:UIControlStateNormal];
        [_validateCodeButton setBackgroundImage:[UIImage imageWithColor:[UIColor yj_grayColor]] forState:UIControlStateHighlighted];
        [_validateCodeButton setBackgroundImage:[UIImage imageWithColor:[UIColor yj_cellBackgroundColor]] forState:UIControlStateDisabled];
    }
    return _validateCodeButton;
}

-(UIButton *)reigstButton{
    if (!_reigstButton) {
        _reigstButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _reigstButton.titleLabel.font = [UIFont systemFontOfSize:18];
        [_reigstButton addTarget:self action:@selector(onRegistButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
        [_reigstButton setTitle:@"注 册" forState:UIControlStateNormal];
        [_reigstButton setTitleColor:[UIColor yj_darkTextColor] forState:UIControlStateNormal];
        _reigstButton.layer.cornerRadius = 5;
        _reigstButton.layer.borderWidth = 0.5;
        _reigstButton.layer.borderColor = [UIColor yj_lineColor].CGColor;
        _reigstButton.layer.masksToBounds = YES;
        [_reigstButton setBackgroundImage:[UIImage imageWithColor:[UIColor whiteColor]] forState:UIControlStateNormal];
        [_reigstButton setBackgroundImage:[UIImage imageWithColor:[UIColor yj_grayColor]] forState:UIControlStateHighlighted];
    }
    return _reigstButton;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
}

@end
